<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateCitaRequest;
use App\Cita;

class CitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Obtengo las fehcas según el día actual.
        //En este ejemplo no se ocupa
        $citas = Cita::whereDate('fecha_hora',date("Y-m-d"))->get()->toArray();

        return response()->json($citas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCitaRequest $request)
    {
        //alamaceno las nuevas citas
        $create = Cita::create($request->all());
        return response()->json(['status' => true, $create ],200);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($fecha)
    {
      //Muestro las citas según una fecha especifiada
      //Se configura hora de inicio y salida de forma manual, dado que es solo para el test, dado que ocupa API REST.
      //Se podría haber realizado un mantenedor de configuración.

      $horaInicio=9;
      $horaSalida=18;
      $dia   = substr($fecha,8,2);
      $mes = substr($fecha,5,2);
      $anio = substr($fecha,0,4);

      //Obtengo el n° de día de la semana que es
      $dia =date('w', mktime(0,0,0,$mes,$dia,$anio));

      //Genero los horararios
      //Esto se podría haber realizado con un calendario, pero el desafío mensionaba que se desplegaban los horarios disponibles y ocupados
      $listHorarios[]= [
        array("fecha"=>$fecha,"dia"=>$dia ,"hora"=>9,"cita"=>null),
        array("fecha"=>$fecha,"dia"=>$dia ,"hora"=>10,"cita"=>null),
        array("fecha"=>$fecha,"dia"=>$dia ,"hora"=>11,"cita"=>null),
        array("fecha"=>$fecha,"dia"=>$dia ,"hora"=>12,"cita"=>null),
        array("fecha"=>$fecha,"dia"=>$dia ,"hora"=>13,"cita"=>null),
        array("fecha"=>$fecha,"dia"=>$dia ,"hora"=>14,"cita"=>null),
        array("fecha"=>$fecha,"dia"=>$dia ,"hora"=>15,"cita"=>null),
        array("fecha"=>$fecha,"dia"=>$dia ,"hora"=>16,"cita"=>null),
        array("fecha"=>$fecha,"dia"=>$dia ,"hora"=>17,"cita"=>null)];

        //Aquí se hace la validación de los sabados y domingos según el n° de día de la semana
      if($dia==6 || $dia==0){

        return response()->json([]);

      }else{

        //Busco las citas según la fecha dada
        $citas = Cita::where('fecha',$fecha)->get();

        //Construyo los horarios ocupados y disponibles
        foreach ($citas as $cita) {

          for($i=0;$i<count($listHorarios[0]);$i++){

           if($listHorarios[0][$i]["hora"]==$cita->hora)
           {
             $listHorarios[0][$i]["cita"]=$cita;
             break;
           }

         }

       }
        return response()->json($listHorarios[0]);
      }



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateCitaRequest $request, $id)
    {
      //Edito los campos de contacto de la cita

      $edit = Cita::find($id)->update($request->all());

      return response()->json($edit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //Elimino una cita específica
      try{
        $delete=Cita::find($id)->delete();
        return response()->json(['status' => true,$delete ],200);

      }catch(\Exception $e){
        return response('Error',500);
      }



     return response()->json($delete);
    }
}
