<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
  protected $table= 'citas';

  protected $fillable = [
      'nombre','telefono','email','fecha','hora','dia_id'
  ];

  public function dia()
  {
    return $this->belongsTo(Dia::class);
  }
}
