# 1. Desafío Asimov

Se realiza API REST

GET|HEAD  | api/citas | citas.index | App\Http\Controllers\CitaController@index 
POST      | api/citas | citas.store | App\Http\Controllers\CitaController@store 
PUT|PATCH | api/citas/{cita} | citas.update | App\Http\Controllers\CitaController@update 
GET|HEAD  | api/citas/{cita} | citas.show | App\Http\Controllers\CitaController@show
DELETE    | api/citas/{cita} | citas.destroy | App\Http\Controllers\CitaController@destroy 


Se realiza cliente en el mismo proyecto, se podría haber realizado separado

En laravel y Vuejs.
Archivo VueJS -> public/js/appController.js


La visualización de las citas se pudo haber realizado mediante un calendario, pero el desafío mencionaba mostrar las horas disponibles.

Por otro lado las horas de inicio y termino de citas, se realizo de forma manual, pero se podría haber hecho un mantenedor para fase 2.


Preguntas A responder:

**● What makes you angry?**

Depender en demasía de otras personas y que estas no cumplan compromisos, por lo cual no poder avanzar en proyectos. Esto me molesta.

**● Jedi or Sith? Why?**

Jedi, siempre al lado de la fuerza, para poder instruir a más aprendices y llevarlos por el buen camino de la programación y desarrollo.
Y realizar las cosas de forma ética.

**● What makes you think that you will not to be replaced and then destroyed by a robot?**

Nada, sinceramente, dado el avance de la inteligencia artificial, la cual me interesa, llegara el punto que las máquinas puedan dominar la tierra, programándose ellas mismas.

**● If we ask your parents about your work, what would they say?**

Mencionarían que están orgullosos por lo que he logrado hasta ahora, realizando lo que me gusta.

**● Are you “the one”? why?**

Humildemente no soy el n° uno, dado que aún me falta por aprender muchas cosas, perfeccionarme y ser un líder jedi.