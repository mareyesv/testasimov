Vue.http.headers.common['X-CSRF-TOKEN'] = Laravel.csrfToken;

var citas= new Vue({
  el: '#citas',
  data: {
    citas: [],
    pagination: {
        total: 0,
        per_page: 2,
        from: 1,
        to: 0,
        current_page: 1
      },
    offset: 4,
    formErrors:{},
    formErrorsUpdate:{},
    cita : {'nombre':'','email':'','fecha':'','telefono':'','hora':0,'dia_id':0}
  },

  computed: {
        isActived: function (){
            return this.pagination.current_page;
        },
        pagesNumber: function (){
            if (!this.pagination.to) {
                return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
                from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
                to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
                pagesArray.push(from);
                from++;
            }
            return pagesArray;
        }
  },

  ready: function () {
    //this.getLocales(this.pagination.current_page);
  },

  methods: {

    getCitas: function() {
      var input = this.cita.fecha;
      this.isProcessing = true;
        this.$http.get('http://localhost:8000/Sistemas/TestAsimov/public/api/citas/'+ input).then(function(response){
          console.log(response.data);
            this.$set('citas', response.data);

        }, function(){
            //error
        });
    },

    showCita: function(cita){
        this.cita.nombre = "";
        this.cita.email = "";
        this.cita.telefono =  "";
        this.cita.hora=cita.hora;
        this.cita.fecha=cita.fecha;
        this.cita.dia_id=cita.dia;
        $("#create-cita").modal('show');
      },

    storeCita: function() {
      var input = this.cita;
		  this.$http.post('http://localhost:8000/Sistemas/TestAsimov/public/api/citas', input).then((response) => {
        $("#create-cita").modal('hide');
        this.changePage(this.pagination.current_page);
        this.cita.nombre = "";
        this.cita.email ="";
        this.cita.telefono = "";
        this.cita.hora=0;
        this.cita.dia_id=0;
		  }, (response) => {
          this.isProcessing = true;
			    this.formErrors = response.data;
	    });
    },

    showEditCita: function(cita){
        this.cita.nombre = cita.cita.nombre;
        this.cita.email =  cita.cita.email;
        this.cita.telefono = cita.cita.telefono;
        this.cita.hora= cita.cita.hora;
        this.cita.id= cita.cita.id;
        this.cita.fecha= cita.cita.fecha;
        this.cita.dia_id= cita.cita.dia;
        $("#edit-cita").modal('show');
      },

    updateCita: function(id){
        this.isProcessing = true;
        var input = this.cita;
        this.$http.put('http://localhost:8000/Sistemas/TestAsimov/public/api/citas/'+ id,input).then((response) => {
            this.changePage(this.pagination.current_page);

            $("#edit-cita").modal('hide');
            $.smallBox({
              title : "Edición Exitosa!. Cita Modificada",
              content : "<i class='fa fa-clock-o'></i> <i>2 segundos atrás...</i>",
              color : "#296191",
              iconSmall : "fa fa-thumbs-up bounce animated",
              timeout : 4000
            });
          }, (response) => {
              this.isProcessing = false;
              $.smallBox({
                title : "Error en la Edición!.",
                content : "<i class='fa fa-times'></i> <i>Intente Nuevamente.</i>",
                color : "#C46A69",
                iconSmall : "fa fa-thumbs-down bounce animated",
                timeout : 4000
              });
              this.formErrorsUpdate = response.data;
          });
      },

    deleteCita: function(cita){
      var vm=this;
      $.SmartMessageBox({
        title : "Eliminar Cita con la Muerte!",
        content : "Desea eliminar la cita?",
        buttons : '[No][Si]'
      }, function(ButtonPressed) {

        if (ButtonPressed === "Si") {
          vm.$http.delete('http://localhost:8000/Sistemas/TestAsimov/public/api/citas/'+cita).then((response) => {
            vm.changePage(vm.pagination.current_page);
              $.smallBox({
                title : "Eliminación Exitosa!. Cita Eliminada",
                content : "<i class='fa fa-clock-o'></i> <i>2 segundos atrás...</i>",
                color : "#296191",
                iconSmall : "fa fa-thumbs-up bounce animated",
                timeout : 4000
              });
          }, (response) => {
              vm.isProcessing = true;
              $.smallBox({
                title : "Error en Eliminación!.",
                content : "<i class='fa fa-times'></i> <i>Intente nuevamente.</i>",
                color : "#C46A69",
                iconSmall : "fa fa-thumbs-down bounce animated",
                timeout : 4000
              });
              vm.formErrors = response.data;
          });
        }
        if (ButtonPressed === "No") {
          $.smallBox({
            title : "Cita no eliminada!",
            content : "<i class='fa fa-clock-o'></i> <i>Decidiste no eliminar la cita...</i>",
            color : "#C46A69",
            iconSmall : "fa fa-times fa-2x fadeInRight animated",
            timeout : 4000
          });
        }

      });

      },

    changePage: function (page) {
          this.pagination.current_page = page;
          this.getCitas();
      }


  }
});
