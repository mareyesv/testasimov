@extends('layouts.app')

@section('content')
<div class="container">
  <div id="citas">
        <div class="row ">
          <div class="col-md-2 ">
            <img src="{{asset('img/lamuerte.png')}}" alt="" />
          </div>
            <div class="col-md-8 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Agenda tu Cita con la Muerte</div>

                    <div class="panel-body">
                    <form enctype="multipart/form-data" v-on:submit.prevent="getCitas"  method="post" class="form-horizontal">
                          <input class="datepicker" type="text" id="fecha" name="fecha" placeholder="::Elije tu fecha" v-model="cita.fecha"    >
                        <button class="btn btn-primary" type="submit" :disabled="isProcessing">
                          <i class="fa fa-eye"></i>
                          Buscar
                        </button>
                      </form>
                        <br><br>
                        <div id="d1" v-if="citas.length > 0">
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th style="text-align: center;">Hora Inicio</th>
                                <th style="text-align: center;">Nombre</th>
                                <th style="text-align: center;">Mail</th>
                                <th style="text-align: center;">Teléfono</th>
                                <th colspan="2">Acción</th>
                            </tr>
                                <tr v-for="cita in citas">
                                    <td style="text-align: center;">@{{ cita.hora }}</td>
                                    <td style="text-align: center;"  v-if="cita.cita != null">@{{ cita.cita.nombre }}</td>
                                    <td style="text-align: center;"  v-else><span class="label label-primary">Disponible</span></td>
                                    <td style="text-align: center;" v-if="cita.cita != null">@{{ cita.cita.email }}</td>
                                    <td style="text-align: center;"  v-else><span class="label label-primary">Disponible</span></td>
                                    <td style="text-align: center;" v-if="cita.cita != null">@{{ cita.cita.telefono }}</td>
                                    <td style="text-align: center;"  v-else><span class="label label-primary">Disponible</span></td>
                                    <td colspan="2" style="text-align: center;" v-if="cita.cita != null">
                                      <button class="edit-modal btn btn-primary"  @click.prevent="showEditCita(cita)">
                                        <i class="fa fa-pencil"></i>
                                      </button>
                                      <button class="edit-modal btn btn-danger" id="smart-mod-eg1"  @click.prevent="deleteCita(cita.cita.id)">
                                        <i class="fa fa-trash"></i>
                                      </button>

                                    </td>
                                    <td style="text-align: center;"  v-else>
                                      <button class="edit-modal btn btn-primary"  @click.prevent="showCita(cita)">
                                        <i class="fa fa-plus-square"></i>
                                      </button>
                                    </td>
                                </tr>
                        </table>
                      </div>
                      <div v-else>
                        <div class="alert alert-info alert-block">
          								<h4 class="alert-heading">Info!</h4>
          								La muerte no acepta citas sábados ni domingos.
          							</div>
                      </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="create-cita"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                  &times;
                </button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-building fa-lg"></i> Nuevo Cita</h4>
              </div>
              <div class="modal-body">
              <form enctype="multipart/form-data" v-on:submit.prevent="storeCita" method="post" class="form-horizontal">
                <input class="form-control" v-model="cita.dia_id" id="dia_id" name="dia_id"  type="hidden">
                <input class="form-control" v-model="cita.fecha" id="fecha" name="fecha"  type="hidden">
                <input class="form-control" v-model="cita.hora" id="hora" name="hora" type="hidden">
                <div class="row">
                  <div class="col-md-11">
                    <div class="form-group">
                      <label class="col-md-2 control-label"><b>Nombre:</b></label>
                      <div class="col-md-8">
                        <input class="form-control" v-model="cita.nombre" id="nombre" name="nombre" placeholder="Nombre" type="text">
                        <span v-if="formErrors['nombre']" class="error text-danger">@{{ formErrors['nombre'] }}</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label"><b>Email:</b></label>
                      <div class="col-md-8">
                        <input class="form-control" v-model="cita.email" id="email" name="email" placeholder="Email" type="text">
                        <span v-if="formErrors['email']" class="error text-danger">@{{ formErrors['email'] }}</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label"><b>Teléfono:</b></label>
                      <div class="col-md-8">
                        <input class="form-control" v-model="cita.telefono"  id="telefono" name="telefono" placeholder="Teléfono" type="text">
                        <span v-if="formErrors['telefono']" class="error text-danger">@{{ formErrors['telefono'] }}</span>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                  Cancelar
                </button>
                <button class="btn btn-primary" type="submit" :disabled="isProcessing">
                  <i class="fa fa-save"></i>
                  Confirmar
                </button>
              </div>
            </div>
            </form>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <!-- Modal -->
        <div class="modal fade" id="edit-cita"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                  &times;
                </button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-building fa-lg"></i> Editar Cita</h4>
              </div>
              <div class="modal-body">
              <form enctype="multipart/form-data" v-on:submit.prevent="updateCita(cita.id)" method="post" class="form-horizontal">
                <input class="form-control" v-model="cita.dia_id" id="dia_id" name="dia_id"  type="hidden">
                <input class="form-control" v-model="cita.fecha" id="fecha" name="fecha"  type="hidden">
                <input class="form-control" v-model="cita.hora" id="hora" name="hora" type="hidden">
                <div class="row">
                  <div class="col-md-11">
                    <div class="form-group">
                      <label class="col-md-2 control-label"><b>Nombre:</b></label>
                      <div class="col-md-8">
                        <input class="form-control" v-model="cita.nombre" id="nombre" name="nombre" placeholder="Nombre" type="text">
                        <span v-if="formErrorsUpdate['nombre']" class="error text-danger">@{{ formErrorsUpdate['nombre'] }}</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label"><b>Email:</b></label>
                      <div class="col-md-8">
                        <input class="form-control" v-model="cita.email" id="email" name="email" placeholder="Email" type="text">
                        <span v-if="formErrorsUpdate['email']" class="error text-danger">@{{ formErrorsUpdate['email'] }}</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label"><b>Teléfono:</b></label>
                      <div class="col-md-8">
                        <input class="form-control" v-model="cita.telefono"  id="telefono" name="telefono" placeholder="Teléfono" type="text">
                        <span v-if="formErrorsUpdate['telefono']" class="error text-danger">@{{ formErrorsUpdate['telefono'] }}</span>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                  Cancelar
                </button>
                <button class="btn btn-primary" type="submit" :disabled="isProcessing">
                  <i class="fa fa-save"></i>
                  Confirmar
                </button>
              </div>
            </div>
            </form>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </div>
</div>
@endsection
